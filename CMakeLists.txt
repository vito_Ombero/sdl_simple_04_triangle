cmake_minimum_required(VERSION 3.9)
project(sdl-prender-opnegl LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED on)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)
set(CMAKE_LIBRARY_PATH ${CMAKE_SOURCE_DIR}/libs)

message("Building engine as shared library...")
add_library(pep SHARED src/pep.cxx src/prender.cxx )

message("Linking libs...")
if(WIN32)   
	target_compile_definitions(sdl-prender-opnegl PRIVATE "-DOM_DECLSPEC=__declspec(dllexport)")
endif(WIN32)

include_directories(${CMAKE_PREFIX_PATH}/include)
find_library(SDL2_LIB NAMES libSDL2-2.0d.so SDL2.dll)

if (MINGW)
	# find out what libraries are needed for staticaly linking with libSDL.a
	# using mingw64 cross-compiler
    
	#$> $ /usr/x86_64-w64-mingw32/sys-root/mingw/bin/sdl2-config --static-libs
	#-L/usr/x86_64-w64-mingw32/sys-root/mingw/lib -lmingw32 -lSDL2main 
	#-lSDL2 -mwindows -Wl,--no-undefined -lm -ldinput8 -ldxguid -ldxerr8 -luser32 
	#-lgdi32 -lwinmm -limm32 -lole32 -loleaut32 -lshell32 -lversion -luuid 
	#-static-libgcc
    
        target_link_libraries(pep
               "${SDL2_LIB}"
               )
	target_link_libraries(pep 
		-lmingw32 
		-lSDL2main 
		-mwindows
		-Wl,--no-undefined
		-lm
		-ldinput8
		-ldxguid
		-ldxerr8
		-luser32 
		-lgdi32
		-lwinmm
		-limm32
		-lole32
		-loleaut32
		-lshell32
		-lversion
		-luuid
		-static-libgcc
		-lopengl32
               )
elseif(UNIX)
	# find out what libraries are needed for staticaly linking with libSDL.a
	# using default linux compiler
	#$> sdl2-config --static-libs
	# -lSDL2 -Wl,--no-undefined -lm -ldl -lpthread -lrt
    
	target_link_libraries(pep 
               "${SDL2_LIB}"
               )
        target_link_libraries(pep
		-lm
		-ldl
		-lpthread
		-lrt
		-lGL
		)
elseif(MSVC)
	find_package(sdl2 REQUIRED)
	target_link_libraries(pep PRIVATE SDL2::SDL2 SDL2::SDL2main opengl32)
endif()

message("Main executable build started!..")
set(game ${PROJECT_NAME}+"_EXEC")
add_executable (game
	src/sdl_simple_03_prender.cxx
)
target_compile_features(game PRIVATE cxx_std_17)
target_link_libraries(game pep)



install(TARGETS pep game
        RUNTIME DESTINATION ${CMAKE_CURRENT_LIST_DIR}/bin
        LIBRARY DESTINATION ${CMAKE_CURRENT_LIST_DIR}/bin
        ARCHIVE DESTINATION ${CMAKE_CURRENT_LIST_DIR}/bin)


if(MSVC)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4 /WX /std:c++17")
else()
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic -Werror")
endif()


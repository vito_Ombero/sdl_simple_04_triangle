#ifndef PRENDER_HXX
#define PRENDER_HXX

#ifndef OM_DECLSPEC
#define OM_DECLSPEC
#endif

#include <fstream>
#include <iosfwd>
#include <iostream>

namespace pep {

struct OM_DECLSPEC vertex {
    float x;
    float y;

    vertex()
        : x(0.f)
        , y(0.f)
    {
    }
};

struct OM_DECLSPEC triangle {

    vertex v[3];

    triangle()
    {
        v[0] = vertex();
        v[1] = vertex();
        v[2] = vertex();
    }
};
std::istream& operator>>(std::istream& is, vertex& v);
std::istream& operator>>(std::istream& is, triangle& t);

class prender {
public:
private:
    prender();
};
}
#endif // PRENDER_HXX

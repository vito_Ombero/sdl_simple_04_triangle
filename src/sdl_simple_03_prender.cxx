//============================================================================
// Name        : sdl_simple_prender.cpp
// Author      : vito.Ombero
// Version     :
// Copyright   : Your copyright notice
// Description : prototiping engine primitive, Ansi-style
//============================================================================

#include <algorithm>
#include <array>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <string_view>

#include "/home/vito/eclipseCosmos/cppOpenGlws/sdl_simple_03_primitive_render/src/pep.hxx"
#include "/home/vito/eclipseCosmos/cppOpenGlws/sdl_simple_03_primitive_render/src/prender.hxx"

int main(int /*argc*/, char* /*argv*/ [])
{

    std::unique_ptr<pep::pengine, void (*)(pep::pengine*)> engine(
        pep::create_engine(), pep::destroy_engine);

    std::string err = engine->initialize("");
    if (!err.empty()) {
        std::cerr << err << std::endl;
        return EXIT_FAILURE;
    }

    bool continue_loop = true;
    while (continue_loop) {
        pep::event event;

        while (engine->read_input(event)) {
            std::cout << event << std::endl;
            switch (event) {
            case pep::event::turn_off:
                continue_loop = false;
                break;
            default:
                break;
            }
        }

        std::ifstream file("vertexes.txt");
        assert(!!file);

        pep::triangle tr;
        file >> tr;

        engine->triangle();

        engine->swap_buffers();
    }

    engine->uninitialize();

    return EXIT_SUCCESS;
}

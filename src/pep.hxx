/*
 * pep.hxx
 *
 *  Created on: Oct 19, 2018
 *      Author: vito
 */
#include "prender.hxx"

#include <array>
#include <string>
#include <string_view>

#include <SDL2/SDL.h>

#ifndef SRC_PEP_HXX_
#define SRC_PEP_HXX_

#ifndef OM_DECLSPEC
#define OM_DECLSPEC
#endif

namespace pep {

enum class event {
    /// input events
    left_pressed,
    left_released,
    right_pressed,
    right_released,
    up_pressed,
    up_released,
    down_pressed,
    down_released,
    select_pressed,
    select_released,
    start_pressed,
    start_released,
    button1_pressed,
    button1_released,
    button2_pressed,
    button2_released,
    /// virtual console events
    turn_off
};

class bindings {

public:
    static std::array<std::string_view, 17> event_names;
    struct bind {
        SDL_Keycode key;
        std::string_view name;
        event event_pressed;
        event event_released;
    };
    static const std::array<bind, 8> keys;

private:
    bindings();
};

std::ostream&
operator<<(std::ostream& stream, const event e);

class pengine;

pengine* create_engine();
void destroy_engine(pengine* e);

class pengine {
public:
    /// create main window
    /// on success return empty string
    virtual std::string initialize(std::string_view config) = 0;
    /// pool event from input queue
    /// return true if event was written
    virtual bool read_input(event& e) = 0;
    virtual void uninitialize() = 0;
    virtual void swap_buffers() = 0;
    virtual ~pengine();
    virtual void triangle(/*const triangle& tt*/) = 0;
};

} /* namespace pep */

#endif /* SRC_PEP_HXX_ */
